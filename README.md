Authors:
Kai DeLorenzo
Diana Lam

Resources:
Man pages

Run with:
```shell
make
LD_PRELOAD=./my-malloc.so ls -lR /
```
Note that 
```shell
ls -lR /
```
can be replaced by any program