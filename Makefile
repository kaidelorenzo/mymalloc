CFLAGS=-g -Wall -pedantic -rdynamic -shared -fPIC

mymalloc: my-malloc.so

test:
	gcc -g -Wall -pedantic -o malloc-test my-malloc-test.c


%.so: %.c
	gcc $(CFLAGS) -o $@ $^

.PHONY: clean
clean:
	rm -f *.so
