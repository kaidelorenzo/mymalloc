#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <pthread.h>

static void *heap_ptr = 0;          //global varible for beginning of the heap
static void *break_ptr;             //ptr to the location of break
static void *first_chunk_ptr;       //ptr to the location of the first allocated chunk of memory
pthread_mutex_t global_malloc_lock; //global thread memory lock


// how much we move the break by
static const size_t CHUNK_SIZE = 4194304; // 4 mebibytes


struct book {
  void *prev_alloc;     // ptr to the previous chunk of memory
  void *next_alloc;     // ptr to beginning of next allocated chunk of memory
  void *end_alloc;      // ptr to end of current chunk of memory
  char filler_bytes[8]; // filler bytes to make sizeof(struct book) % 16 == 0
};

void *malloc (size_t size);
void free(void *ptr);
void *calloc(size_t nmemb, size_t size);
void *realloc(void *ptr, size_t size);

void *reallocarray(void *ptr, size_t nmemb, size_t size);
int posix_memalign(void **memptr, size_t alignment, size_t size);
void *aligned_alloc(size_t alignment, size_t size);
void *valloc(size_t size);
void *memalign(size_t alignment, size_t size);
void *pvalloc(size_t size);
size_t malloc_usable_size (void *ptr);



void *malloc (size_t size){
  if(size == 0){
    return NULL;
  }
  // rounding size up to multiple of 16
  if(size % 16 != 0){
    size = size + (16 - (size % 16));
  }
  // lock
  pthread_mutex_lock(&global_malloc_lock);
  // allocation when there are no other live allocations
  if (heap_ptr == 0){
    heap_ptr = sbrk(0);
    if (heap_ptr == (void *)-1){
      // unlock
      pthread_mutex_unlock(&global_malloc_lock);
      return NULL;
    }
    break_ptr = (char *)heap_ptr + 10000000;

    int break_factor = ((size + sizeof(struct book)) / CHUNK_SIZE) + 1;
    break_ptr = (char*)break_ptr + CHUNK_SIZE * break_factor;
    if (brk(break_ptr) == -1){
      // unlock
      pthread_mutex_unlock(&global_malloc_lock);
      return NULL;
    }

    first_chunk_ptr = (char *)heap_ptr + sizeof(struct book);
    struct book *inital_book = (struct book *)first_chunk_ptr - 1;
    inital_book->end_alloc = (char *)first_chunk_ptr + size;
    inital_book->next_alloc = NULL;
    inital_book->prev_alloc = NULL;

    // unlock
    pthread_mutex_unlock(&global_malloc_lock);
    return first_chunk_ptr;
  }
  struct book *prev_book;
  struct book *new_book;
  struct book *next_book;
  void *new_alloc;
  size_t free_space = (char *)first_chunk_ptr - sizeof(struct book) - (char *)heap_ptr;

  // if there is enough space between first chunk ptr and beginning of heap
  if (free_space >= sizeof(struct book) + size){
  //if (0){
    next_book = (struct book *)first_chunk_ptr - 1;
    new_book = (struct book*)heap_ptr;
    new_book->prev_alloc = NULL;
    new_book->next_alloc = first_chunk_ptr;
    first_chunk_ptr = new_book + 1;
    new_alloc = first_chunk_ptr;
    next_book->prev_alloc = new_alloc;
    new_book->end_alloc = (char *)new_alloc + size;
  } 
  // else search for location
  else{
    struct book *current_book = (struct book*)first_chunk_ptr - 1;

    while(current_book->next_alloc){
      
      free_space = ((char *)(current_book->next_alloc) - sizeof(struct book)) - (char *)(current_book->end_alloc);
      if(free_space >= sizeof(struct book) + size){
        // if there is enough free space we can allocate space there
        break;
      }

      // if not, keep checking to see if there is free space and if not, add allocated memory to the end
      current_book = ((struct book*)(current_book->next_alloc)) - 1;
    }
    // increase brk if necessary
    size_t space_until_break = (char *)break_ptr - (char *)current_book->end_alloc;
    if(size + sizeof(struct book) > space_until_break){
      size_t extra_space_needed = size + sizeof(struct book) - space_until_break;
      int break_factor =  (extra_space_needed / CHUNK_SIZE) + 1;
      break_ptr = (char*)break_ptr + CHUNK_SIZE * break_factor;
      if (brk(break_ptr) == -1){
        // unlock
        pthread_mutex_unlock(&global_malloc_lock);
        return NULL;
      }
    }
    
    // update linked list pointers
    prev_book = current_book;
    new_book = prev_book->end_alloc;

    if(prev_book->next_alloc){
      next_book = (struct book *)(prev_book->next_alloc) - 1;
      next_book->prev_alloc = new_book + 1;
    }
    new_book->next_alloc = prev_book->next_alloc;
    new_book->prev_alloc = prev_book + 1;
    new_alloc = new_book + 1;
    prev_book->next_alloc = new_alloc;
    new_book->end_alloc = (char *)new_alloc + size;
  }
  // unlock
  pthread_mutex_unlock(&global_malloc_lock);
  return new_alloc;
}

void free(void *ptr){
  if (ptr == NULL){
    return;
  }
  // lock
  pthread_mutex_lock(&global_malloc_lock);
  struct book *next_book;
  struct book *prev_book;
  struct book *current_book = (struct book *)ptr - 1;
  
  // if this is the last allocated chunk then reset heap
  if (ptr == first_chunk_ptr && !current_book->next_alloc){
    if (brk(heap_ptr) == -1){
        // unlock
        pthread_mutex_unlock(&global_malloc_lock);
        return;
      }
    heap_ptr = 0;
  }
  // if there is only one allocated chunk
  else if (ptr == first_chunk_ptr){
    next_book = ((struct book *)(current_book->next_alloc)) - 1;
    next_book->prev_alloc = NULL;
    first_chunk_ptr = next_book + 1;
  } 
  // if last chunk
  else if (!current_book->next_alloc){
    prev_book = ((struct book *)(current_book->prev_alloc)) - 1;
    prev_book->next_alloc = NULL;
  } 
  // if in the middle of the list
  else {
    prev_book = ((struct book *)(current_book->prev_alloc)) - 1;
    next_book = ((struct book *)(current_book->next_alloc)) - 1;
    prev_book->next_alloc = next_book + 1;
    next_book->prev_alloc = prev_book + 1;
  }
  // unlock
  pthread_mutex_unlock(&global_malloc_lock);
}

void *calloc(size_t nmemb, size_t size){
  if (!nmemb || !size){
    return NULL;
  }
  size_t new_size = nmemb * size;
  // check for overflow
  if (new_size / nmemb != size) {
    return NULL;
  }
  if(new_size % 16 != 0){
    new_size = new_size + (16 - (new_size % 16));
    // check to see if rounding overflows
    if (new_size < 0) {
      return NULL;
    }
  }

  void *ptr = malloc(new_size);
  if (!ptr){
    return NULL;
  }
  // lock
  pthread_mutex_lock(&global_malloc_lock);
  memset(ptr, 0, new_size);
  // unlock
  pthread_mutex_unlock(&global_malloc_lock);
  return ptr;
}

void *realloc(void *ptr, size_t size){
  // rounding size up to multiple of 16
  if (ptr == NULL){
    return malloc(size);
  }
  if (size == 0){
    free(ptr);
    return NULL;
  }
  if(size % 16 != 0){
    size = size + (16 - (size % 16));
  }
  // lock
  pthread_mutex_lock(&global_malloc_lock);
  struct book *current_book = ((struct book*)ptr) - 1;
  size_t amount_to_copy = (char *)(current_book->end_alloc) - (char *)ptr;
  /*if (amount_to_copy > size){
    amount_to_copy = size;
  }*/
  if (size <= amount_to_copy){
    (((struct book*)ptr) - 1)->end_alloc = (char *)ptr + size;
    // unlock
    pthread_mutex_unlock(&global_malloc_lock);
    return ptr;
  }
  // unlock
  pthread_mutex_unlock(&global_malloc_lock);
  void *new_ptr = malloc(size);
  if (!new_ptr){
    return NULL;
  }
  // lock
  pthread_mutex_lock(&global_malloc_lock);
  memcpy(new_ptr, ptr, amount_to_copy);
  // unlock
  pthread_mutex_unlock(&global_malloc_lock);
  free(ptr);
  return new_ptr;
}

/* other malloc functions needed for some applications */

void *reallocarray(void *ptr, size_t nmemb, size_t size){
  // check for overflow
  size_t new_size = nmemb * size;
  if (new_size / nmemb != size) {
    return NULL;
  }
  if(new_size % 16 != 0){
    new_size = new_size + (16 - (new_size % 16));
    // check to see if rounding overflows
    if (new_size < 0) {
      return NULL;
    }
  }
  return realloc(ptr, new_size);
}

// doesn't work if there is no prior malloc call
int posix_memalign(void **memptr, size_t alignment, size_t size){
  if (size == 0){
    *memptr = NULL;
    return 0;
  }
  if(size % alignment != 0){
    size = size + (alignment - (size % alignment));
  }
  // lock
  pthread_mutex_lock(&global_malloc_lock);
  struct book *current_book = (struct book*)first_chunk_ptr - 1;
  while(current_book->next_alloc){
    current_book = ((struct book*)(current_book->next_alloc)) - 1;
  }

  struct book *prev_book = current_book;
  struct book *new_book = prev_book->end_alloc;
  void *new_alloc = new_book + 1;
  if ((uint64_t)new_alloc % alignment != 0){
    // round up to alignment
    new_alloc = (void *)((uint64_t)new_alloc + (alignment - ((uint64_t)new_alloc % alignment)));
    new_book = ((struct book *)new_alloc) - 1;
  }

  size_t total_size_needed = size + sizeof(struct book) + ((uint64_t)new_book - (uint64_t)prev_book->end_alloc);

  // increase break if necessary
  size_t space_until_break = (char *)break_ptr - (char *)prev_book->end_alloc;
  if(total_size_needed > space_until_break){
    size_t extra_space_needed = total_size_needed - space_until_break;
    int break_factor =  (extra_space_needed / CHUNK_SIZE) + 1;
    break_ptr = (char*)break_ptr + CHUNK_SIZE * break_factor;
    if (brk(break_ptr) == -1){
      // unlock
      pthread_mutex_unlock(&global_malloc_lock);
      return -1;
    }
  }

  new_book->next_alloc = prev_book->next_alloc;
  new_book->prev_alloc = prev_book + 1;
  prev_book->next_alloc = new_alloc;
  new_book->end_alloc = (char *)new_alloc + size;
  *memptr = new_alloc;
  // unlock
  pthread_mutex_unlock(&global_malloc_lock);
  
  return 0;
}

// the following are unimplemented and will write to stout to signal that a program tried to use the function
void *aligned_alloc(size_t alignment, size_t size){
  write(1, "aligned_alloc\n", 14);
  return NULL;
}
void *valloc(size_t size){
  write(1, "valloc\n", 7);
  return NULL;  
}
void *memalign(size_t alignment, size_t size){
  write(1, "memalign\n", 8);
  return NULL;
}
void *pvalloc(size_t size){
  write(1, "pvalloc\n", 8);
  return NULL;
}
size_t malloc_usable_size (void *ptr){
  write(1, "malloc_usable_size\n", 19);
  return 0;
}